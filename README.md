
Brix Project Documentation: The project contains two components
One component manages a whitelist of authorized ip addresses including server and client sides
.A second component allows sending emails only to the authorized ip addresses

To use this repository, you should folow some steps:
1. Clone the BrixServerr repository to empty folder.
2. Execute the script.sql file in BrixDB repository.
3. open the mailAPI solution and set the ipAPI project as startUp project.
4. Change the connection string in the appsetting of the ipAPI project.
5. Go to https://BrachaLudmir@bitbucket.org/m3brix/brixserverr.git and clone it.
Send email
1. set the  mailAPI project as startUp project in the mailAPI solution.
2. Open CheckSendMail, It's in another solution so that it's possible to run them both together and see how it work.


White IP List management
Communication between the server side and the client is done by protocol Http:
Server
We implemented the three-layer model that
allows you to replace or upgrade each layer independently of the other layers.
DAL-Data Access Layer:
access to the DB by ADO.Net,
Contains functions that execute the procedures stored in the database,
The functions get SqlParameter parameter list
And return a dataSet object
BLL-Business Logic Layer:
Performs proper checks on the data returning from the DAL
GUI-Grafic User Interface:
Written in Angular 7.
Displays the address list and lets you add delete and edit.

We also built a number of class libraries:
Factory:
Accepts a type of interface and create an instance of the class that implements it, in order to avoid unnecessary dependency between layers,
The factory loads all the desired dll files in Assembly,
When we want to create a specific class type object, we send to the factory the interface type that the class type is implement, for a function that, by reflection, research the loaded dll files and finds the class type that implements it,
The function save  in a dictionary for each interface type the class that implements it.
IDAL:
Contains an interface for each class in the DAL layer.

The factory and interfaces class libraries prevent unnecessary dependency between layers, allowing Web Api to recognize only the interfaces.

DTO - Contains classes for the types of objects that pass between the server and the client, to prevent the circulation problem created by transferring data from the database to the client.
 
Convert:
This library converts between data types,
Sql procedures accept sqlParameter parameters and return data into a dataSet object.
This class library contains two generic functions, the first converting all types of data coming from the client side to sqlParameter,
And the second converts the dataSet object to the desired data type to return to the client.



erver send Email:

This server sends e-mails and allows sending only to authorized IP
addresses from the white liste stored in the system.

This server is built in the above layers.

When contacting the server, the IP address of the router from which a
request for sending mail arrived, is retrieved.

In the DTO layer, we built a class similar to the MailMessage object.

In the conversion layer, we added a class that converts from a DTO
object to a microsoft mailMessage object that sends the email via an
SMTP server.

The DTO and conversion were built to create a more convenient
infrastructure for moving the mail object from server to server like
it should work in practice












