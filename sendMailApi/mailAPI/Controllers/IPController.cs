﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Convert;
using System.Net.Mail;
using IDAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using BLL;
using static mailAPI.Startup;
using Microsoft.Extensions.Options;
using Factory;
using MailMessage;

namespace BrixApi.Controllers
{
    [Route("Mailapi/[controller]")]
    [ApiController]
    public class IPController : ControllerBase
    {
        private ConnectionStringsConfig _connectionStrings;

        public IPController(IOptionsSnapshot<ConnectionStringsConfig> connectionStrings)
        {
            _connectionStrings = connectionStrings?.Value ?? throw new ArgumentNullException(nameof(connectionStrings));

        }
        // GET: api/IP
        [Route("{action}")]
        [HttpPost]
        public SendResponse sendMail(SendRequest sendRequest)
        {
            string IP = new string(Dns.GetHostEntry(Dns.GetHostName()).AddressList.GetValue(1).ToString());
            IDal dal = factory.Resolve<IDal>() as IDal;
            //if(dd.isExist(d))
            SendResponse sendResponse = new SendResponse();
            bool isexist = dal.IsWhiteIP(Convert<string>.DtoToSqlParameters(IP), _connectionStrings.DatabaseConnection);
            if (isexist==true)
            {
               System.Net.Mail.MailMessage m = converteMeilMessage.ToMailMessage(sendRequest);
                try
                {
                    new mailGun().sendEmail(m);
                    sendResponse.IsSent = true;
                }
                catch(Exception ex) {

                    Console.WriteLine("email not sent" + ex.Message);
                }
               
            }
            else
                sendResponse.IsSent = false;
            return sendResponse;
        }

    }
}
