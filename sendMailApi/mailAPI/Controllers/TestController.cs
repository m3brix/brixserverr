﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using IDAL;
using Factory;
using Convert;
using DTO;
using System.Data.SqlClient;
using static mailAPI.Startup;
using Microsoft.Extensions.Options;
using System.Net;

using System.Net.Mail;
using BLL;
using MailMessage;

namespace mailAPI.Controllers
{
  


    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private ConnectionStringsConfig _connectionStrings;

        public TestController(IOptionsSnapshot<ConnectionStringsConfig> connectionStrings)
        {
            _connectionStrings = connectionStrings?.Value ?? throw new ArgumentNullException(nameof(connectionStrings));

        }
      
        [HttpGet]
        [Route("{action}")]
        public string testFactory()
        {
            try
            {
                string s = AppDomain.CurrentDomain.BaseDirectory + @"dlls";
                factory.LoadAssemblies(AppDomain.CurrentDomain.BaseDirectory + @"dlls");

                return s;
            }
            catch (Exception e)
            {
                return e.Message;
            }

            //var res = idal.getWhiteList();
            //return res.Tables[0].Rows[0].ItemArray[1].ToString();
            //List<whiteListDTO> whiteList = Convert<whiteListDTO>.DataSetToDTO(res);
            //return whiteList[0].ip;
            //    bool e = idal.testDal();
            //return e;


        }
        [HttpGet]
        [Route("{action}")]
        public string testConnection()
        {
            try
            {
                SqlConnection c = new SqlConnection();
                c.ConnectionString = _connectionStrings.DatabaseConnection;
                c.Open();
                return "true";

            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        [HttpGet]
        [Route("{action}")]
        public string testIp()
        {
            string IP = new string(Dns.GetHostEntry(Dns.GetHostName()).AddressList.GetValue(1).ToString());
            return IP;
        }
        [HttpGet]
        [Route("{action}")]
        public ActionResult<bool> testIsWhiteIp()
        {
            IDal idal = Factory.factory.Resolve<IDal>() as IDal;
            string IP = new string(Dns.GetHostEntry(Dns.GetHostName()).AddressList.GetValue(1).ToString());
            bool r = idal.IsWhiteIP(Convert<string>.DtoToSqlParameters(IP),_connectionStrings.DatabaseConnection);
            return r;
        }

        [HttpGet]
        [Route("{action}")]
        public bool testSendingMail(SendRequest sendRequest )
        {
            string IP = new string(Dns.GetHostEntry(Dns.GetHostName()).AddressList.GetValue(1).ToString());
            IDal dal = factory.Resolve<IDal>() as IDal;
            //if(dd.isExist(d))
            bool r = dal.IsWhiteIP(Convert<string>.DtoToSqlParameters(IP), _connectionStrings.DatabaseConnection);
            if (dal.IsWhiteIP(Convert<string>.DtoToSqlParameters(IP), _connectionStrings.DatabaseConnection))
            {
                //IDAL.mailMessageDTO mailMassage = new factory().Resolve<IDAL.mailMessageDTO>() as IDAL.mailMessageDTO;

                var mail = new DTO.mailMessageDTO();

                System.Net.Mail.MailMessage m = converteMeilMessage.ToMailMessage(sendRequest);

                try
                {
                    new mailGun().sendEmail(m);
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
            return false;
        }

    }
}