﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace IDAL
{
    public interface IDal
    {
        bool testDal(string connectionString);
        int AddFailedIP(SqlParameter[] lp, string connectionString);
        int AddWhiteIP(SqlParameter[] lp, string connectionString);
        int DeleteFailedIp(SqlParameter[] lp, string connectionString);
        int DeleteWhiteIp(SqlParameter[] lp, string connectionString);
        int EditIP(SqlParameter[] lp, string connectionString);
        bool IsWhiteIP(SqlParameter[] lp, string connectionString);
        DataSet getWhiteList(string connectionString);
    }
}

