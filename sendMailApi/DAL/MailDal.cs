﻿using System;
using System.Data.SqlClient;
using System.Data;
using IDAL;

namespace DAL
{
    public class MailDal :IDal
    {
        public bool testDal(string connectionString)
        {
            try
            {
                MyDB db = new MyDB();
                db.openDb(connectionString);
                return true;
            }
            catch
            {
                return false;
            }

        }
        public int AddFailedIP(SqlParameter[] lp, string connectionString)
        {
            try
            {
                string spName = @"dbo.[addFailedIP]";
                MyDB db = new MyDB();
                db.openDb(connectionString);
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                int res = cmd.ExecuteNonQuery();
                db.cn.Close();
                return res;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }


        public int AddWhiteIP(SqlParameter[] lp, string connectionString)
        {
            try
            {
                string spName = @"dbo.[addWhiteIP]";
                MyDB db = new MyDB();
                db.openDb(connectionString);
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                int res = cmd.ExecuteNonQuery();
                db.cn.Close();
                return res;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }

        public int DeleteFailedIp(SqlParameter[] lp, string connectionString)
        {
            try
            {
                string spName = @"dbo.[deleteFailedIp]";
                MyDB db = new MyDB();
                db.openDb(connectionString);
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                int res = cmd.ExecuteNonQuery();
                db.cn.Close();
                return res;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }

        public int DeleteWhiteIp(SqlParameter[] lp, string connectionString)
        {
            try
            {
                string spName = @"dbo.[deleteWhiteIp]";
                MyDB db = new MyDB();
                db.openDb(connectionString);
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                int res = cmd.ExecuteNonQuery();
                db.cn.Close();
                return res;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }

        public int EditIP(SqlParameter[] lp, string connectionString)
        {
            try
            {
                string spName = @"dbo.[editDescreption]";
                MyDB db = new MyDB();
                db.openDb(connectionString);
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                int res = cmd.ExecuteNonQuery();
                db.cn.Close();
                return res;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }

        public DataSet getWhiteList(string connectionString)
        {
            try
            {
                string spName = @"dbo.[getWhiteList1]";
                MyDB db = new MyDB();
                db.openDb(connectionString);
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                //SqlCommandBuilder.DeriveParameters(cmd);
                //for (int i = 0; i < lp.Length; i++)
                //    cmd.Parameters[i + 1].Value = lp[i].Value;
                //int res = cmd.ExecuteNonQuery();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public bool IsWhiteIP(SqlParameter[] lp, string connectionString)
        {
            try
            {
                string spName = @"dbo.[isWhiteIP]";
                MyDB db = new MyDB();
                db.openDb(connectionString);
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                //int res = cmd.ExecuteNonQuery();
                var res = cmd.ExecuteScalar();
                bool exists = res != null ? (int)res > 0 : false;
                db.cn.Close();
                return exists;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }


    }
}