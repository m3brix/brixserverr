﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DAL
{
    class MyDB
    {
        public SqlConnection cn;

        public void openDb(string connectionString)
        {
            cn = new SqlConnection();
            cn.ConnectionString = connectionString;
            cn.Open();
        }
    }
}
