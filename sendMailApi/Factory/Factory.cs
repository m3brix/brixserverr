﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Factory
{
//    public class Factory
//    {

//        //public List<Type> classes
//        //{
//        //    get { return classes; }
//        //    set { if (classes == null) classes = new List<Type>(); }
//        //}


//        private static List<Type> classes;
//        private static Dictionary<Type, Type> dicTypes;
//        private static Assembly assembly { get; set; }

//        //public List<Type> getClasses()
//        //{
//        //    if (classes == null)
//        //        classes = new List<Type>();
//        //    return classes;
//        //}
//        //public Dictionary<Type, Type> getDicTypes()
//        //{
//        //    if (dicTypes == null)
//        //        dicTypes = new Dictionary<Type, Type>();
//        //    return dicTypes;
//        //}

//        public static void LoadAssemblies(string pathToDll)
//        {
//            string[] files = Directory.GetFiles(pathToDll, "*.dll", SearchOption.AllDirectories);
//            foreach (string file in files)
//            {
//                assembly = Assembly.LoadFrom(file);
//                classes.AddRange(assembly.GetTypes().Where(t => t.IsClass));
//            }
//        }

//        public object Resolve<T>()
//        {
//            Type thisInterface = typeof(T);
//            if (dicTypes.ContainsKey(thisInterface) == false)
//            {
//                Type thisClass = classes.FirstOrDefault(c => c.GetInterface(thisInterface.Name) != null);
//                dicTypes[thisInterface] = thisClass;
//            }
//            return Activator.CreateInstance(dicTypes[thisInterface]);
//        }
//    }
//}

public static class factory
{

    public static List<Type> classes = new List<Type>();
    public static Dictionary<Type, Type> dicTypes = new Dictionary<Type, Type>();
    public static Assembly a { get; set; }

    public static void LoadAssemblies(string p)
    {
        //DicTypes = new Dictionary<Type, Type>();
        //var u = AppDomain.CurrentDomain.GetAssemblies().Where(x=>x.FullName.Contains("DrawingApi"));
        var files = Directory.GetFiles(p, "*.dll", SearchOption.AllDirectories);
        foreach (string file in files)
        {
            a = Assembly.LoadFrom(file);
            classes.AddRange(a.GetTypes().Where(c => c.IsClass));
        }


    }

        public static object Resolve<T>()
        {

            Type t = typeof(T);
            dicTypes[t] = classes.FirstOrDefault(c => c.GetInterface(t.ToString()) != null);
            var Instance = Activator.CreateInstance(dicTypes[t]);
            return Instance;
        }
    }

}

