﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace DTO
{
    public class mailMessageDTO
    {
       public string from { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public List<string> listTo;
        public List<string> listAttchment;

    }
}
