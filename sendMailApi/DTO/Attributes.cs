﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{

    [AttributeUsage(AttributeTargets.Property)]
    public class identityAttribute : Attribute { }

    [AttributeUsage(AttributeTargets.Class)]
    public class DtoAttribute : Attribute { }
}
