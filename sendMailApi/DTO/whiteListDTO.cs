﻿using System;

namespace DTO
{
    [Dto]

    public class whiteListDTO
    {
        [identity]
        public int id { get; set; }
        public string ip { get; set; }
        public string description { get; set; }
        
    }
}
