﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using DTO;
using MailMessage;

namespace Convert
{
    public class converteMeilMessage
    {
        public static System.Net.Mail.MailMessage ToMailMessage(SendRequest mail)
        {
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            message.Body = mail.body;
            message.Subject = mail.subject;
            message.From = new MailAddress(mail.from);
            foreach (var item in mail.listTo)
            {
                message.To.Add(item);
            }
            if (mail.listAttchment != null)
            {
                foreach (var item in mail.listAttchment)
                {
                    message.Attachments.Add(new Attachment(item));
                }
            }
            return message;
        }

    }
}
